░░░░░░   ░░░░░░  ░░░░░░░░ ░░░░░░░ ░░ ░░      ░░░░░░░ ░░░░░░░  
▒▒   ▒▒ ▒▒    ▒▒    ▒▒    ▒▒      ▒▒ ▒▒      ▒▒      ▒▒       
▒▒   ▒▒ ▒▒    ▒▒    ▒▒    ▒▒▒▒▒   ▒▒ ▒▒      ▒▒▒▒▒   ▒▒▒▒▒▒▒  
▓▓   ▓▓ ▓▓    ▓▓    ▓▓    ▓▓      ▓▓ ▓▓      ▓▓           ▓▓  
██████   ██████     ██    ██      ██ ███████ ███████ ███████  

# dotfiles

Dotfiles with settings for unix environments.  

Mainly customized for use on Fedora, with leftovers from mac / ubuntu environments  

This iteration is called *Zweihander* because the current setup is focused on touch typing.  
All work is or should be done with the keyboard with a mission of using the mouse as little as possible.  

Window manager of choice is currently BSPWM. The colors are custom, but will always be WIP.
Config for I3 and Awesomewm are still in there for when I want to play with them. 

These dotfiles are a bit messy because I keep experimenting with new tools for improving my workflow.  
I would love to have it configured so that I can vim everything. 
