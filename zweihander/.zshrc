#!/bin/zsh


# zsh completions
fpath=(/usr/local/share/zsh-completions $fpath)

# prompt
#PROMPT="%m|%*|%F{yellow}%d%f => "

# Specify default editor. Possible values: vim, nano, ed etc.
export EDITOR=vim

# For brew, at least
export PATH=/usr/local/bin:$PATH

# NVM Stuff
export NVM_DIR="$HOME/.nvm"
. "$(brew --prefix nvm)/nvm.sh"


# Enable Base 16 shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

### Added by Zplugin's installer
source '/Users/nkrul/.zplugin/bin/zplugin.zsh'
autoload -Uz _zplugin
(( ${+_comps} )) && _comps[zplugin]=_zplugin
### End of Zplugin installer's chunk
fpath+=${ZDOTDIR:-~}/.zsh_functions

# plugins

zplugin snippet OMZ::plugins/colored-man-pages/colored-man-pages.plugin.zsh     # Colored man pages
zplugin ice depth=1; zplugin light romkatv/powerlevel10k			# powerlevel 10k
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
