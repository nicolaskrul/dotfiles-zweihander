#!/bin/bash

ZTEMP=$HOME/.ztmp

mkdir $ZTEMP
cd $ZTEMP 

# git clone https://github.com/davatorium/rofi.git
# cd rofi
# git submodule update --init
# autoreconf -i
# meson setup build
# ninja -C build
# sudo ninja -C build install

# cd ..

# git clone --recursive https://github.com/lcpz/awesome-copycats.git
# mv -bv awesome-copycats/* ~/.config/awesome && rm -rf awesome-copycats

git clone https://github.com/yshui/picom.git
cd picom
git submodule update --init --recursive
meson --buildtype=release . build
ninja -C build
sudo ninja -C build install
