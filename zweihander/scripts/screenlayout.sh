#!/bin/sh
xrandr --output DVI-D-0 --mode 1920x1080 --pos 0x136 --rotate normal --output HDMI-1-1 --off --output VGA-1-1 --off --output HDMI-0 --mode 1920x1080 --pos 5360x136 --rotate normal --output DP-5 --off --output DP-4 --primary --mode 3440x1440 --pos 1920x0 --rotate normal --output DP-3 --off --output DP-2 --off --output DP-1 --off --output DP-0 --off
