###
# RpmFusion Free Repo
# This is holding only open source, vetted applications - fedora just cant legally distribute them themselves thanks to
# Software patents
###

# sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

###
# RpmFusion NonFree Repo
# This includes Nvidia Drivers and more
###

if [ ! -z "$NONFREE" ]; then
	sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
fi


###
# Force update the whole system to the latest and greatest
###

sudo dnf upgrade --best --allowerasing --refresh -y

###
# Install custom packages
###

sudo dnf install \
-y \
# applications
awesome \
ffmpeg \
keepassxc \
krita \
neovim \
zsh \
qbittorrent \
jack-audio-connection-kit \
dmenu \
rxvt-unicode \
terminus-fonts.noarch \
flatpak \
linux-util-user \
htop \
vlc \
bat \
kitty \
# development libraries and tools
xcb-util-devel \
xcb-util-keysyms-devel \
xcb-util-wm-devel \
alsa-lib-devel \
# Rofi deps
glib2-devel \
meson \
cmake \
cairo-devel \
pango-devel \
libxkbcommon-devel \
libxkbcommon-x11-devel \
librsvg2-devel \
libjpeg-turbo-devel \
xcb-util-xrm-devel \
startup-notification-devel \
xkeyboard-config-devel \
flex \
bison \
# picom deps
# libXext-devel \
# libxcb1-devel \
# libxcb-damage0-devel \
# libxcb-xfixes0-devel \
# libxcb-shape0-devel \
# libxcb-render-util0-devel \
# libxcb-render0-devel \
# libxcb-randr0-devel \
# libxcb-composite0-devel \
# libxcb-image0-devel \
# libxcb-present-devel \
# libxcb-xinerama0-devel \
# libxcb-glx0-devel \
# libpixman-1-devel \
# libdbus-1-devel \
# libconfig-devel \
# libgl1-mesa-devel \
# libpcre2-devel \
# libevdev-devel \
# uthash-devel \
libev-devel \
libx11-xcb-devel \
xcb-util-renderutil-devel \
xcb-util-image-devel \
uthash-devel \
libconfig-devel \
mesa-libGL-devel \
libdbusmenu-devel
libxcb-devel \
xcb-util-keysyms-devel \
xcb-util-devel \
xcb-util-wm-devel \
xcb-util-xrm-devel \
yajl-devel \
libXrandr-devel \
startup-notification-devel \
libev-devel \
xcb-util-cursor-devel \
libXinerama-devel \
libxkbcommon-devel \
libxkbcommon-x11-devel \
pcre-devel \
pango-devel \
i3status \
i3lock

# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
# sudo flatpak install flathub com.spotify.Client
