#!/bin/bash

./install_packages.sh
./install_repos.sh
./symlinks.sh

sudo cp -R $HOME/zweihander/scripts/services/ etc/systemd/system/
sudo chmod 644 /etc/systemd/system/startup.service

