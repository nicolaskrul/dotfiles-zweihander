#!/bin/zsh
#===============================================================================
#
#             NOTES: For this to work you must have symlinked the github
#                    repo to your home folder as ~/zweihander/
#
#===============================================================================

#==============
# Variables
#==============
DOTFILES=~/zweihander
CONFIG_DIR=~/zweihander log_file=~/install_log.txt
#==============
# Create symlinks in the home folder
# Allow overriding with files of matching names in the custom-configs dir
#==============
ln -s $CONFIG_DIR/Xresources ~/Xresources

mkdir ~/.config/bspwm
mkdir ~/.config/sxhkd
ln -s $CONFIG_DIR/bspwm/ ~/.config/bspwm
ln -s $CONFIG_DIR/sxhkd/ ~/.config/sxhkd

ln -s $CONFIG_DIR/redshift/redshift.conf ~/.config/
ln -s $CONFIG_DIR/rofi/ ~/.config/rofi

ln -s $CONFIG_DIR/vim ~/.vim
ln -s $CONFIG_DIR/vim/.vimrc ~/.vimrc

# Theme
ln -s $DOTFILES/colors/gtk.css ~/.config/gtk-3.0/
ln -s $DOTFILES/colors/gtkrc-2.0 ~/.gtkrc-2.0
ln -s $DOTFILES/colors/base16-zweihander.vim ~/.vim/plugged/base16-vim/colors/
ln -s $CONFIG_DIR/dunst ~/.config/


#==============
# Set zsh as the default shell
#==============
sudo chsh -s /bin/zsh

#==============
# Give the user a summary of what has been installed
#==============
echo -e "\n====== Summary ======\n"
cat $log_file
echo
rm $log_file

