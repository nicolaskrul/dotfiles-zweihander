#!/usr/bin/env bash
set -eu

# requires:
# lemonbar-xft (lemonbar with a patch that allows xft fonts)
# nerd fonts (a font set with a patch that includes the icons)
# bspc from bspwm
# wpa_cli from wpasupplicant
# bluetooth from tlp
# openvpn
# amixer from alsa-utils
# xprop from x11-utils

readonly NOTIFY_PIPE='/tmp/lemonbar'
readonly DARK_BLACK='#202936'
readonly BACKGROUND='#141920'
readonly FOREGROUND='#F2AC59'
readonly DARK_WHITE='#d1c6bc'
readonly DARK_CYAN='#376a91'
readonly DARK_MAGENTA='#7569e0'
readonly HEIGHT='60'
readonly FONT='ProggyCleanTT Nerd Font Mono:style=Book:size=16'

# bspwm ○ ●


# display the current desktop
# I II III IV V VI VII VIII IX X
groups() {
	cur=`xprop -root _NET_CURRENT_DESKTOP | awk '{print $3}'
	tot=`xprop -root _NET_NUMBER_OF_DESKTOPS | awk '{print $3}'
	# Desktop numbers start at 0. if you want desktop 2 to be in second place,
	# start counting from 1 instead of 0. But wou'll lose a group ;)
	for w in `seq 0 $((cur - 1))`; do line="${line}="; done
	# enough =, let's print the current desktop
	line="${line}|"
	# En then the other groups
	for w in `seq $((cur + 2)) $tot`; do line="${line}="; done
	# don't forget to print that line!
	echo $line
}

# choose an icon based on node/window class and title
function get_icon() {
    if [[ -n ${node_id} ]]; then
        case $(xprop -id ${node_id} WM_CLASS | awk -F '"' '{print $4}' | tr '[[:upper:]]' '[[:lower:]]') in
            feh)
                echo ''
                ;;
            firefox | vivaldi-stable)
                echo ''
                ;;
            gimp)
                echo ''
                ;;
            gparted)
                echo ''
                ;;
            libreoffice)
                echo ''
                ;;
            mpv)
                echo ''
                ;;
            oomox)
                echo ''
                ;;
            synaptic)
                echo ''
                ;;
            kitty)
                case $(xprop -id ${node_id} WM_NAME | awk -F '"' '{print $2}' | tr '[[:upper:]]' '[[:lower:]]') in
                    abook | khard)
                        echo ''
                        ;;
                    cmus | cmus\ *)
                        echo ''
                        ;;
                    khal)
                        echo ''
                        ;;
                    mutt)
                        echo ''
                        ;;

                    # rangers window title is just python
                    ranger | python)
                        echo ''
                        ;;
                    vi | vim | nvim)
                        echo ''
                        ;;
                    *)
                        echo ''
                        ;;
                esac
                ;;
            virtualbox)
                echo ''
                ;;
            *)
                echo ''
                ;;
        esac
    fi
}

function get_title() {
    local node_class
    if [[ -n ${node_id} ]]; then
        readonly node_class=$(xprop -id ${node_id} WM_CLASS | awk -F '"' '{print $4}' | tr '[[:upper:]]' '[[:lower:]]')
        case "${node_class}" in
            urxvt)
                readonly node_title=$(xprop -id ${node_id} WM_NAME | awk -F '"' '{print $2}' | tr '[[:upper:]]' '[[:lower:]]')
                case "${node_title}" in

                    # rangers window title is just python
                    python)
                        echo 'ranger'
                        ;;
                    nvim | vim | vi)
                        echo 'vim'
                        ;;
                    *)
                        echo "${node_title}"
                        ;;
                esac
                ;;
            vivaldi-stable)
                echo 'vivaldi'
                ;;
            *)
                echo "${node_class}"
                ;;
        esac
    fi
}

function bspwm_workspace() {
    local mon_id desk_id
    while read -r mon_id; do
        while read -r desk_id; do
            if [[ ${desk_id} = $(bspc query --desktops --desktop) ]]; then
                echo -n "%{B${DARK_BLACK}}%{F${FOREGROUND}}"
            else
                echo -n "%{B-}%{F-}"
            fi
            if bspc query --desktop ${desk_id} --nodes --node .leaf &> /dev/null; then
                echo -n "  "
            else
                echo -n "  "
            fi
        done <<< $(bspc query --monitor ${mon_id} --desktops)
    done <<< $(bspc query --monitors)
    echo '%{B-}%{F-}'
}

function bspwm_window() {
    local node_id
    while read -r node_id; do
        if [[ -n ${node_id} ]]; then
            if [[ ${node_id} = $(bspc query --nodes --node) ]]; then
                echo -n "%{B${DARK_BLACK}} %{F${FOREGROUND}}$(get_icon) $(get_title) "
            elif [[ ${node_id} = $(bspc query --nodes --node .leaf.hidden) ]]; then
                echo -n "%{B-}%{F-} $(get_icon) "
            else
                echo -n "%{B-} %{F${FOREGROUND}}$(get_icon) %{F-} $(get_title) "
            fi
        fi
    done <<< $(bspc query --desktop --nodes --node .window)
    echo '%{B-}%{F-}'
}

# battery ↯

function battery() {
    local battery_path battery_percentage battery_status
    readonly battery_path='/sys/class/power_supply/BAT0'
    battery_percentage=$(< "${battery_path}/capacity")
    if (( battery_percentage > 100 )); then
        battery_percentage=100
    fi
    readonly battery_status=$(< "${battery_path}/status")
    echo -n ' '
    if [[ "${battery_status}" = 'Charging' ]] && (( battery_percentage > 79 )); then
        echo -n "%{F${DARK_CYAN}}"
    elif [[ "${battery_status}" = 'Charging' ]]; then
        echo -n "%{F${FOREGROUND}}"
    elif (( battery_percentage > 80 )); then
        echo -n "%{F${FOREGROUND}}"
    elif (( battery_percentage > 60 )); then
        echo -n "%{F${FOREGROUND}}"
    elif (( battery_percentage > 40 )); then
        echo -n "%{F${FOREGROUND}}"
    elif (( battery_percentage > 20 )); then
        echo -n "%{F${FOREGROUND}}"
    else
        echo -n "%{F${DARK_MAGENTA}}"

    fi
    echo "%{F-} ${battery_percentage} "
}

# brightness ☼ ☀ *

function brightness() {
    local brightness_raw brightness_max brightness
    readonly brightness_raw=$(< '/sys/class/backlight/intel_backlight/brightness')
    readonly brightness_max=$(< '/sys/class/backlight/intel_backlight/max_brightness')
    readonly brightness=$(printf '%.f\n' $(bc <<< "scale=0; ${brightness_raw} * 100 / ${brightness_max}"))
    echo -n " %{F${FOREGROUND}}"
    if (( brightness > 30 )); then
        echo -n ''
    elif (( brightness > 15 )); then
        echo -n ''
    else
        echo -n ''
    fi
    echo "%{F-} ${brightness} "
}

# time

function clock() {
    echo "%{B${DARK_BLACK}} %{F${FOREGROUND}} $(date '+%H:%M') %{B-}%{F-}"
}

# date

function calendar() {
    echo -n " %{F${FOREGROUND}}%{F-} "
    date '+%a %b %d '
}

# connection  

function connection() {
    local lan_path lan_status wifi_raw wifi_status wifi_ssid wifi_signal
    if pgrep -x openvpn &> /dev/null; then
        echo -n " %{F${FOREGROUND}}%{F-} on "
    fi
    # if [[ $(bluetooth | awk '{print $3}') = 'on' ]]; then
        # echo -n " %{F${FOREGROUND}}%{F-} on "
    # fi
    readonly lan_path='/sys/class/net/wlp59s0@/operstate'
    if [[ -f "${lan_path}" ]]; then
        readonly lan_status=$(< "${lan_path}")
        if [[ -n "${lan_status}" ]]; then
            echo -n " %{F${FOREGROUND}}%{F-} ${lan_status} "
        fi
    fi
    echo -n " %{F${FOREGROUND}}%{F-} "
    readonly wifi_raw=$(wpa_cli status)
    readonly wifi_status=$(awk -F '=' '/^wpa_state=/{print tolower($2)}' <<< "${wifi_raw}")
    if [[ "${wifi_status}" = 'completed' ]]; then
        readonly wifi_ssid=$(awk -F '=' '/^ssid=/{print $2}' <<< "${wifi_raw}")

        # 0% ~ -100; 100% ~ -30
        readonly wifi_signal_raw=$(wpa_cli scan_result | awk "/${wifi_ssid}/"'{print $3}')
        readonly wifi_signal=$(bc <<< "scale=0; 100 - sqrt((${wifi_signal_raw} + 30)^2) * 100 / 70")
        echo -n "${wifi_signal} ${wifi_ssid}"
    elif [[ -n "${wifi_status}" ]]; then
         echo -n "${wifi_status}"
    else
        echo -n 'error'
    fi
    echo ' '
}

Network(){
     s=$(ip addr | awk '/state UP/ {print $2}')
     INTERFACE=$(echo -n ${s:0:-1})
     R1=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`
     T1=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`
     sleep 1
     R2=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`
     T2=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`
     TBPS=`expr $T2 - $T1`
     RBPS=`expr $R2 - $R1`
     TKBPS=`expr $TBPS / 1024`
     RKBPS=`expr $RBPS / 1024`
     line="    "
     echo -n "$INTERFACE UP: $(printf "${line:${#TKBPS}}""%s %s" $TKBPS)kB/s DOWN: $(printf "${line:${#RKBPS}}""%s %s" $RKBPS)kB/s"
     
}

# wifi
function Wifi(){
    WIFISTR=$( iwconfig wlp59s0 | grep "Link" | sed 's/ //g' | sed 's/LinkQuality=//g' | sed 's/\/.*//g')
    if [ ! -z $WIFISTR ] ; then
        WIFISTR=$(( ${WIFISTR} * 100 / 70))
        ESSID=$(iwconfig wlp59s0 | grep ESSID | sed 's/ //g' | sed 's/.*://' | cut -d "\"" -f 2)
        if [ $WIFISTR -ge 1 ] ; then
           echo -e "%{F${FOREGROUND}} %{F-}${ESSID} %{F${FOREGROUND}} %{F-}${WIFISTR}%"
        fi
    fi
}
# volume ⊓ ⊄ ⋐ ⊂

function volume() {
    local volume_percent volume_status volume_output
    volume_percent="$(amixer -D pulse get Master | awk -F '\\[|%' '/Front Left:/{print $2}')"
    echo -n " %{F${FOREGROUND}}"
    if amixer -D pulse get Master | grep 'off' &> /dev/null; then
        volume_percent='off'
        echo -n ''
    elif pactl list sinks | grep 'Active Port: analog-output-headphones' &> /dev/null; then
        echo -n ''
    elif (( volume_percent > 25 )); then
        echo -n ''
    else
        echo -n ''
    fi
    echo "%{F-} ${volume_percent} "
}

# main

function main() {
    local line notify i clock calendar Wifi battery brightness volume workspace window
    line=''
    notify=''

    # link descriptor 7 with my notification pipe to use read on the pipe; workaround for a kind of non-blocking read on the named pipe
    exec 7<>"${NOTIFY_PIPE}"

    for (( i = 0; i >= 0; i++ )); do
        if [[ -n "${line}" || -n "${notify}" ]]; then
            case "${line}" in
                WARN*)
                    notify="${line:4}"
                    echo "%{Sl}%{c}%{F${DARK_MAGENTA}}${notify}"
                    sleep 60 && echo 'CLEAR' > "${NOTIFY_PIPE}" &
                    ;;
                HINT*)
                    notify="${line:4}"
                    echo "%{Sl}%{c}%{F${DARK_CYAN}}${notify}"
                    sleep 10 && echo 'CLEAR' > "${NOTIFY_PIPE}" &
                    ;;
                SHOW*)
                    notify="${line:4}"
                    echo "%{Sl}%{c}${notify}"
                    sleep 2 && echo 'CLEAR' > "${NOTIFY_PIPE}" &
                    ;;
                CLEAR)
                    notify=''
                    ;;
            esac
        else
            if (( i == 3600 )); then
                i=1
            fi
            if (( i % 60 == 0 )); then
                clock="$(clock)"
            fi
            if (( i % 3600 == 0 )); then
                calendar="$(calendar)"
                #weather="$(weather)"
            fi
            if (( i % 15 == 0 )); then
                Wifi="$(Wifi)"
                battery="$(battery)"
            fi
            if (( i % 2 == 0)); then
                brightness="$(brightness)"
                volume="$(volume)"
            fi
            workspace="$(bspwm_workspace)"
            window="$(bspwm_window)"
            echo -e "%{Sl}%{l}${workspace}${window}%{c}%{r}${brightness}${volume}${Wifi}${battery}${calendar}${clock}"
        fi

        # load notification from pipe or wait one second
        read -r -t 1 line <& 7 || true
    done | lemonbar \
        -d -g "x${HEIGHT}" -a 0 -u 0 \
        -B "${BACKGROUND}" \
        -F "${DARK_WHITE}" \
        -U "${DARK_WHITE}" \
        -f "${FONT}"
}

# fresh pipe
if [[ -e "${NOTIFY_PIPE}" ]]; then
    rm "${NOTIFY_PIPE}"
fi
mkfifo "${NOTIFY_PIPE}"

case "$@" in
    '')
        main
        ;;
    *)
        echo 'Bad arguments'
        exit 1
        ;;
esac

