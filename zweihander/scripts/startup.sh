#!/bin/bash

# swap caps with esc
setxkbmap -option caps:escape

# Run picom
picom -C -b --experimental-backends
