#!/bin/zsh
export PRIMARY_DISPLAY="$(xrandr | awk '/primary/{print $1}')"

# Custom scripts to be launched after login
mullvad &

# Font settings
xset +fp /home/plakband/.local/share/fonts
xset fp rehash

# Set monitor layout
bash ~/Development/archer/merlin/scripts/screenlayout.sh

# Launch polybar

#killall -q polybar
#polybar main &
. ~/.config/polybar/launch.sh &

# load Xresources
xrdb ~/.Xresources

# redshift
redshift-gtk &

# xidlelock + i3lock for automatically lock screen

# Only exported variables can be used within the timer's command.

# Run xidlehook
#xidlehook \
#  `# Don't lock when there's a fullscreen application` \
#  --not-when-fullscreen \
#  `# Don't lock when there's audio playing` \
#  --not-when-audio \
#  `# Dim the screen after 60 seconds, undim if user becomes active` \
#  --timer normal 1350 \
#    'xrandr --output "$PRIMARY_DISPLAY" --brightness .1' \
#    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' \
#  `# Undim & lock after 20 more seconds` \
#  --timer primary 20 \
#    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; i3lock' \
#    '' \
#  `# Finally, suspend an hour after it locks` \
#  --timer normal 3600 \
#    'systemctl suspend' \
#    ''

feh --bg-fill ~/Pictures/Wallpapers/3wsplklh38m31.jpg

