"  ░░░░░  ░░    ░░ ░░░░░░░░  ░░░░░░   ░░░░░░  ░░░░░░  ░░░    ░░░ ░░░    ░░░  ░░░░░  ░░░    ░░ ░░░░░░  ░░░░░░░ 
" ▒▒   ▒▒ ▒▒    ▒▒    ▒▒    ▒▒    ▒▒ ▒▒      ▒▒    ▒▒ ▒▒▒▒  ▒▒▒▒ ▒▒▒▒  ▒▒▒▒ ▒▒   ▒▒ ▒▒▒▒   ▒▒ ▒▒   ▒▒ ▒▒      
" ▒▒▒▒▒▒▒ ▒▒    ▒▒    ▒▒    ▒▒    ▒▒ ▒▒      ▒▒    ▒▒ ▒▒ ▒▒▒▒ ▒▒ ▒▒ ▒▒▒▒ ▒▒ ▒▒▒▒▒▒▒ ▒▒ ▒▒  ▒▒ ▒▒   ▒▒ ▒▒▒▒▒▒▒ 
" ▓▓   ▓▓ ▓▓    ▓▓    ▓▓    ▓▓    ▓▓ ▓▓      ▓▓    ▓▓ ▓▓  ▓▓  ▓▓ ▓▓  ▓▓  ▓▓ ▓▓   ▓▓ ▓▓  ▓▓ ▓▓ ▓▓   ▓▓      ▓▓ 
" ██   ██  ██████     ██     ██████   ██████  ██████  ██      ██ ██      ██ ██   ██ ██   ████ ██████  ███████ 

" read file from disc when vim regains focus
" au FocusGained,BufEnter * :silent! !
"
" Run black for python files on save
autocmd BufWritePre *.py execute ':Black'

autocmd FileType json syntax match Comment +\/\/.\+$+ 
