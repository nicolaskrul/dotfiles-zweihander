"  ░░░    ░░░  ░░░░░  ░░░░░░  ░░░░░░  ░░ ░░░    ░░  ░░░░░░  ░░░░░░░ 
"  ▒▒▒▒  ▒▒▒▒ ▒▒   ▒▒ ▒▒   ▒▒ ▒▒   ▒▒ ▒▒ ▒▒▒▒   ▒▒ ▒▒       ▒▒      
"  ▒▒ ▒▒▒▒ ▒▒ ▒▒▒▒▒▒▒ ▒▒▒▒▒▒  ▒▒▒▒▒▒  ▒▒ ▒▒ ▒▒  ▒▒ ▒▒   ▒▒▒ ▒▒▒▒▒▒▒ 
"  ▓▓  ▓▓  ▓▓ ▓▓   ▓▓ ▓▓      ▓▓      ▓▓ ▓▓  ▓▓ ▓▓ ▓▓    ▓▓      ▓▓ 
"  ██      ██ ██   ██ ██      ██      ██ ██   ████  ██████  ███████ 


" Leader
map ; <Leader>

" jk -> Esc
" inoremap <C-L> <Esc>
imap <silent> jf <Esc>

" tabs
nnoremap <silent> th  :tabprev<CR>
nnoremap <silent> tj  :tabprev<CR>
nnoremap <silent> tl  :tabnext<CR>
nnoremap <silent> tk  :tabnext<CR>
nnoremap <silent> tt  :tabedit<Space>
nnoremap <silent> tn  :tabnew<CR>
nnoremap <silent> tm  :tabm<Space>
nnoremap <silent> td  :tabclose<CR>

" window navigation
nnoremap <silent> <C-h> <C-w>h
nnoremap <silent> <C-j> <C-w>j
nnoremap <silent> <C-k> <C-w>k
nnoremap <silent> <C-l> <C-w>l

" Go to definition using ALE, gD is useless
nnoremap <silent>gD :ALEGoToDefinition<CR>

" open nerdtree
nnoremap <silent><C-n> :NERDTreeToggle %<CR>
nnoremap <silent><leader>n :NERDTreeFind<CR>

" session management
nnoremap <leader>so :OpenSession!<Space>
nnoremap <leader>ss :SaveSession<Space>
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>

" remove search highlight
nnoremap <silent>// :noh<CR>

" force reload
nnoremap <silent><leader>e :e!<CR>

" Replace word
nnoremap <leader>r :%s/\<<C-r><C-w>\>//g<left><left>
vnoremap <leader>r "hy:%s/<C-r>h//gc<left><left><left>

" FZF
nnoremap <silent><C-p> :Files<Cr>
nnoremap <silent><leader>f :F<Cr>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Gundo
nnoremap <silent><Leader>u :GundoToggle<CR>

" JS console log selection
nnoremap <silent><Leader>ll :put! =printf('console.log(''%s:'', %s);', expand('<cword>'), expand('<cword>'))<CR>

" theming
" copy colors from palette css file
nnoremap <leader>zy 1gg3w<C-v>5l15j"my

" pase colors into theme file
nnoremap <leader>zp 16gg4w<C-v>5l15j"mp

nnoremap <leader>wh :VimwikiAll2HTML


" Disable default keymappings
let g:zettel_default_mappings = 0 
" This is basically the same as the default configuration
augroup filetype_vimwiki
  autocmd!
  autocmd FileType vimwiki nmap <silent><leader>zn <Plug>ZettelNew
  autocmd FileType vimwiki imap <silent> [[ [[<esc><Plug>ZettelSearchMap
  autocmd FileType vimwiki nmap T <Plug>ZettelYankNameMap
  autocmd FileType vimwiki xmap z <Plug>ZettelNewSelectedMap
  autocmd FileType vimwiki nmap gZ <Plug>ZettelReplaceFileWithLink
  autocmd Filetype vimwiki nmap zt <Plug>ZettelGenerateTags
augroup END


" Vim zettel
