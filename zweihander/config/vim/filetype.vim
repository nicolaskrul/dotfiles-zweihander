" ░░░░░░░ ░░ ░░      ░░░░░░░ ░░░░░░░░ ░░    ░░ ░░░░░░  ░░░░░░░ 
" ▒▒      ▒▒ ▒▒      ▒▒         ▒▒     ▒▒  ▒▒  ▒▒   ▒▒ ▒▒      
" ▒▒▒▒▒   ▒▒ ▒▒      ▒▒▒▒▒      ▒▒      ▒▒▒▒   ▒▒▒▒▒▒  ▒▒▒▒▒   
" ▓▓      ▓▓ ▓▓      ▓▓         ▓▓       ▓▓    ▓▓      ▓▓      
" ██      ██ ███████ ███████    ██       ██    ██      ███████ 


augroup filetypedetect
autocmd BufNewFile,BufRead .vimrc set syntax=bash
augroup END
