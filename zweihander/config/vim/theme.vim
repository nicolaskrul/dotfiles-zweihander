" ░░░░░░░░ ░░   ░░ ░░░░░░░ ░░░    ░░░ ░░░░░░░ 
"    ▒▒    ▒▒   ▒▒ ▒▒      ▒▒▒▒  ▒▒▒▒ ▒▒      
"    ▒▒    ▒▒▒▒▒▒▒ ▒▒▒▒▒   ▒▒ ▒▒▒▒ ▒▒ ▒▒▒▒▒   
"    ▓▓    ▓▓   ▓▓ ▓▓      ▓▓  ▓▓  ▓▓ ▓▓      
"    ██    ██   ██ ███████ ██      ██ ███████ 


" Use base 16 theme
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif
colorscheme base16-zweihander
" let g:airline_theme = "base16_zweihander"
"colorscheme base16-gruvbox-dark-medium
" source ~/.vim/plugged/vim-airline-themes/autoload/airline/themes/base16-zweihander-airline.vim
