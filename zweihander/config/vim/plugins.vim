" ░░░░░░  ░░      ░░    ░░  ░░░░░░  ░░ ░░░    ░░ ░░░░░░░
" ▒▒   ▒▒ ▒▒      ▒▒    ▒▒ ▒▒       ▒▒ ▒▒▒▒   ▒▒ ▒▒
" ▒▒▒▒▒▒  ▒▒      ▒▒    ▒▒ ▒▒   ▒▒▒ ▒▒ ▒▒ ▒▒  ▒▒ ▒▒▒▒▒▒▒
" ▓▓      ▓▓      ▓▓    ▓▓ ▓▓    ▓▓ ▓▓ ▓▓  ▓▓ ▓▓      ▓▓
" ██      ███████  ██████   ██████  ██ ██   ████ ███████

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" generic
Plug 'scrooloose/nerdtree'						                        " NerdTree
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'				        " NerdTree syntax highlight
Plug 'nathanaelkane/vim-indent-guides'					              " Indent guides
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'				                                " Fzf fuzzy finder

Plug 'xolox/vim-misc'                                         " session
Plug 'xolox/vim-session'
Plug 'vimwiki/vimwiki'                                        " vimwiki
Plug 'michal-h21/vim-zettel'                                  " Zettelkasten vimwiki extension
Plug 'sjl/gundo.vim'                                          " graphical undo overview
Plug '907th/vim-auto-save'
Plug 'yuttie/comfortable-motion.vim'
Plug 'sgur/vim-editorconfig'

" themes
Plug 'chriskempson/base16-vim'							                  " Theme
Plug 'vim-airline/vim-airline'						                    " Vim airline
Plug 'vim-airline/vim-airline-themes'				  	              " Vim Airline themes
Plug 'mhinz/vim-startify'

" Development
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'dense-analysis/ale'						                          " ALE (async linter)
Plug 'mhinz/vim-signify'					 	                          " Show git changes
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'		      				                    " Surround
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }         " Markdown support
Plug 'scrooloose/nerdcommenter'						                    " Easy comments
Plug 'ludovicchabant/vim-gutentags'
"Plug 'SirVer/ultisnips'
Plug 'airblade/vim-rooter'                                    " Set working dir automatically to project dir
" Plug 'prettier/vim-prettier', {
  " \ 'do': 'yarn install',
    " \ 'for': [
      " \ 'javascript',
      " \ 'typescript',
      " \ 'css',
      " \ 'less',
      " \ 'scss',
      " \ 'json',
      " \ 'graphql',
      " \ 'markdown',
      " \ 'vue',
      " \ 'lua',
      " \ 'php',
      " \ 'python',
      " \ 'ruby',
      " \ 'html', ] }

" javascript
Plug 'pangloss/vim-javascript', { 'for': ['javascript, typescript'] }  " Vim javascript
Plug 'maxmellon/vim-jsx-pretty', { 'for': ['javascript, typescript'] } " Vim jsx pretty
Plug 'HerringtonDarkholme/yats.vim', {'for': ['javascript, typescript'] }	   		     
" Plug 'eslint/eslint', { 'for': ['javascript, typescript'] }	   		     " Eslint
" Plug 'eslint/eslint'
" Plug 'leafgarland/typescript-vim', { 'for': ['javascript, typescript'] }
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

"Plug 'mlaursen/vim-react-snippets'

" css
Plug 'ap/vim-css-color', 	{ 'for': ['scss', 'css'] }                   " css color preview
Plug 'hail2u/vim-css3-syntax', { 'for': ['scss', 'css'] }              " css 3 syntax

" other
Plug 'mattn/emmet-vim', { 'for': ['html', 'htm'] }                     " html
Plug 'jparise/vim-graphql'                                             " GraphQl
Plug 'tpope/vim-haml', { 'for': 'haml' }
Plug 'OmniSharp/omnisharp-vim', { 'for': 'csharp' }                    " C#
Plug 'psf/black', {'for': 'python' }

Plug 'gorodinskiy/vim-coloresque'
"
" loaded last per requirement
Plug 'ryanoasis/vim-devicons'                                          " icons based on filetype


call plug#end()



" Startify Header
let g:startify_custom_header = [
 \'',
 \'    ░░░░░░░ ░░     ░░ ░░░░░░░ ░░ ░░   ░░  ░░░░░  ░░░    ░░ ░░░░░░  ░░░░░░░ ░░░░░░  ',
 \'       ▒▒▒  ▒▒     ▒▒ ▒▒      ▒▒ ▒▒   ▒▒ ▒▒   ▒▒ ▒▒▒▒   ▒▒ ▒▒   ▒▒ ▒▒      ▒▒   ▒▒ ',
 \'      ▒▒▒   ▒▒  ▒  ▒▒ ▒▒▒▒▒   ▒▒ ▒▒▒▒▒▒▒ ▒▒▒▒▒▒▒ ▒▒ ▒▒  ▒▒ ▒▒   ▒▒ ▒▒▒▒▒   ▒▒▒▒▒▒  ',
 \'     ▓▓▓    ▓▓ ▓▓▓ ▓▓ ▓▓      ▓▓ ▓▓   ▓▓ ▓▓   ▓▓ ▓▓  ▓▓ ▓▓ ▓▓   ▓▓ ▓▓      ▓▓   ▓▓ ',
 \'    ███████  ███ ███  ███████ ██ ██   ██ ██   ██ ██   ████ ██████  ███████ ██   ██ ',
 \'',
 \]
" #################################################################
" ############################# DEVICONS ##########################
" #################################################################


" after a re-source, fix syntax matching issues (concealing brackets):
if exists('g:loaded_webdevicons')
    call webdevicons#refresh()
endif

" NERDTrees File highlighting
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
 exec 'autocmd FileType nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
 exec 'autocmd FileType nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

" NERDTrees File highlighting only the glyph/icon
" test highlight just the glyph (icons) in nerdtree:
autocmd filetype nerdtree highlight js_icon ctermbg=none ctermfg=Red guifg=#f4dd4b
autocmd filetype nerdtree highlight jsx_icon ctermbg=none ctermfg=Red guifg=#81d8f7
autocmd filetype nerdtree highlight ts_icon ctermbg=none ctermfg=Red guifg=#3178c1
autocmd filetype nerdtree highlight tsx_icon ctermbg=none ctermfg=Red guifg=#81d8f7
autocmd filetype nerdtree highlight html_icon ctermbg=none ctermfg=Red guifg=#ffa500
autocmd filetype nerdtree highlight go_icon ctermbg=none ctermfg=Red guifg=#ffa500
autocmd filetype nerdtree highlight css_icon ctermbg=none ctermfg=Red guifg=#3370ad

" #################################################################
" ############################### ALE #############################
" #################################################################

" Set linters for specific syntax
let b:ale_linters = {
  \ 'python': ['flake8', 'black'],
  \ 'javascript': ['eslint', 'prettier'],
  \ 'typescript': ['prettier'],
  \ 'scss': ['prettier'],
  \ 'css': ['prettier'],
  \ 'html': ['prettier'], }

" ### ALE config
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
let g:ale_fix_on_save = 1

" 
" ### prettier config
" FORMATTERS
"au FileType javascript setlocal formatprg=prettier
"au FileType javascript.jsx setlocal formatprg=prettier
"au FileType typescript setlocal formatprg=prettier\ --parser\ typescript
"au FileType html setlocal formatprg=js-beautify\ --type\ html
"au FileType scss setlocal formatprg=prettier\ --parser\ css
"au FileType css setlocal formatprg=prettier\ --parser\ css

" fix on save async
"let g:prettier#quickfix_enabled = 0

"let g:prettier#autoformat = 0
"autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync


" #################################################################
" ######################### FZF and ripgrep #######################
" #################################################################

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%'  }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

  " Enable per-command history.
  " CTRL-N and CTRL-P will be automatically bound to next-history and
  " previous-history instead of down and up. If you don't like the change,
  " explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
 let g:fzf_history_dir = '~/.local/share/fzf-history'"
 "
" Search file contents,
" - uses ripgrep
let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
    \ -g "*.{ts,tsx,js,jsx,go,json,php,md,styl,jade,css,scss,sass,less,html,config,py,hs,rb,conf,yml,tml}"
    \ -g "!{.git,backend/node_modules,node_modules,dist,vendor}/*" '

" - excludes filenames:  {'options': '--delimiter : --nth 4..'}
command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)


" Vim sessions
let g:session_autosave = 'no'

" #################################################################
" ############################ NERD TREE ##########################
" #################################################################

let g:NERDTreeWinSize=60

let g:WebDevIconsNerdTreeBeforeGlyphPadding = ''
let g:WebDevIconsUnicodeDecorateFolderNodes = v:true

" enable line numbers
let NERDTreeShowLineNumbers=1
"
" make sure relative line numbers are used
autocmd FileType nerdtree setlocal relativenumber

" #################################################################
" ######################### NERD commenter ########################
" #################################################################

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
let g:NERDTrimTrailingWhitespace = 1

" #################################################################
" ######################### Git Fugitive ##########################
" #################################################################

" display errors in airline
let g:airline#extensions#ale#enabled = 1

" #################################################################
" ########################### Airline #############################
" #################################################################
"
" use nerd font symbols
let g:airline_powerline_fonts = 1

" show buffer
let g:airline#extensions#tabline#enabled = 1
" show buffer number
let g:airline#extensions#tabline#buffer_nr_show = 1

" Only show filenames on buffer title
let g:airline#extensions#tabline#formatter = 'unique_tail'

" #################################################################
" #################### Comfortable Motion #########################
" #################################################################

let g:comfortable_motion_friction = 199.0
let g:comfortable_motion_air_drag = 3.0


" #################################################################
" ############################# CoC ###############################
" #################################################################

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

nnoremap <silent> <space>d :<C-u>CocList diagnostics<cr>

let g:coc_global_extensions = ['coc-eslint', 'coc-tsserver', 'coc-tslint', 'coc-emmet', 'coc-css', 'coc-html', 'coc-json', 'coc-yank', 'coc-prettier', 'coc-tailwindcss']

" #################################################################
" ########################## Prettier #############################
" #################################################################

" Max line length that prettier will wrap on: a number or 'auto' (use
" textwidth).
" default: 'auto'
let g:prettier#config#print_width = 'auto'

" number of spaces per indentation level: a number or 'auto' (use
" softtabstop)
" default: 'auto'
let g:prettier#config#tab_width = 'auto'

" use tabs instead of spaces: true, false, or auto (use the expandtab setting).
" default: 'auto'
let g:prettier#config#use_tabs = 'auto'

" flow|babylon|typescript|css|less|scss|json|graphql|markdown or empty string
" (let prettier choose).
" default: ''
let g:prettier#config#parser = ''

" cli-override|file-override|prefer-file
" default: 'file-override'
let g:prettier#config#config_precedence = 'file-override'

" always|never|preserve
" default: 'preserve'
let g:prettier#config#prose_wrap = 'preserve'

" css|strict|ignore
" default: 'css'
let g:prettier#config#html_whitespace_sensitivity = 'css'

" false|true
" default: 'false'
let g:prettier#config#require_pragma = 'false'

" #################################################################
" ############################# VIMWIKI  ##########################
" #################################################################
let researchWiki = {
  \ 'auto_export': 1,
  \ 'path':'$HOME/Dropbox/vimwiki/research/',
	\ 'path_html': '$HOME/Dropbox/vimwiki/research/html',
  \ 'automatic_nested_syntaxes':1,
  \ 'ext':'.md',
  \ 'syntax':'markdown',
	\ 'custom_wiki2html': '$HOME/.vim/wiki2html.sh',
	\ 'template_path': '$HOME/.vim/templates/',
	\ 'template_default': 'html5',
	\ 'template_ext': '.html'}

let devWiki = {
  \ 'auto_export': 1,
  \ 'path':'$HOME/Dropbox/vimwiki/dev',
	\ 'path_html': '$HOME/Dropbox/vimwiki/dev/html',
  \ 'automatic_nested_syntaxes':1,
  \ 'ext':'.md',
  \ 'syntax':'markdown',
	\ 'custom_wiki2html': '$HOME/.vim/wiki2html.sh',
	\ 'template_path': '$HOME/.vim/templates/',
	\ 'template_default': 'html5',
	\ 'template_ext': '.html'}

let phandelver = {
  \ 'auto_export': 1,
  \ 'path':'$HOME/Dropbox/Phandelver',
	\ 'path_html': '$HOME/Dropbox/Phandelver/html',
  \ 'automatic_nested_syntaxes':1,
  \ 'ext':'.txt',
  \ 'syntax':'markdown',
	\ 'custom_wiki2html': '$HOME/.vim/wiki2html.sh',
	\ 'template_path': '$HOME/.vim/templates/',
	\ 'template_default': 'html5',
	\ 'template_ext': '.html'}

" Settings for Vimwiki
let g:vimwiki_list = [researchWiki, devWiki, phandelver]

let g:taskwiki_sort_orders={"C": "pri-"}
let g:taskwiki_syntax = 'markdown'
let g:taskwiki_markdown_syntax='markdown'
let g:taskwiki_markup_syntax='markdown'

let g:zettel_options = [{"template" :  "$HOME/.vim/templates/zettel.tpl"}]
