" ░░    ░░ ░░ ░░░    ░░░ ░░░░░░   ░░░░░░ 
" ▒▒    ▒▒ ▒▒ ▒▒▒▒  ▒▒▒▒ ▒▒   ▒▒ ▒▒      
" ▒▒    ▒▒ ▒▒ ▒▒ ▒▒▒▒ ▒▒ ▒▒▒▒▒▒  ▒▒      
"  ▓▓  ▓▓  ▓▓ ▓▓  ▓▓  ▓▓ ▓▓   ▓▓ ▓▓      
"   ████   ██ ██      ██ ██   ██  ██████ 


set modifiable
filetype plugin on
set encoding=UTF-8
set lazyredraw


" tabs and spaces
set shiftwidth=2
set ts=2 	" tabstop
set sts=2	" softTabstop
set et		" expand tabs to spaces

"" Improve speed
set ttyfast

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Disable compatibility
set nocompatible
set hidden

" Diable swapfiles
set noswapfile

" Enable term true colors
syn on
set termguicolors
set background=dark
let base16colorspace=256

" Relative numbers and absolute number for line 0
set number relativenumber

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" hide mode (airline displays it better)
set noshowmode

let g:ale_completion_tsserver_autoimport = 1

" Enable autosave with vim-auto-save plugin
let g:auto_save = 1  " enable AutoSave on Vim startup

" Mappings
source ~/.vim/mappings.vim

" Plugins
source ~/.vim/plugins.vim

" theme
source ~/.vim/theme.vim

" Functions
source ~/.vim/functions.vim

" Autocommands
source ~/.vim/autocommands.vim
