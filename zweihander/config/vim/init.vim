" ░░░    ░░ ░░    ░░ ░░ ░░░    ░░░ 
" ▒▒▒▒   ▒▒ ▒▒    ▒▒ ▒▒ ▒▒▒▒  ▒▒▒▒ 
" ▒▒ ▒▒  ▒▒ ▒▒    ▒▒ ▒▒ ▒▒ ▒▒▒▒ ▒▒ 
" ▓▓  ▓▓ ▓▓  ▓▓  ▓▓  ▓▓ ▓▓  ▓▓  ▓▓ 
" ██   ████   ████   ██ ██      ██ 


set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc


" Basic configuration
syntax on
set ruler               " Show the line and column numbers of the cursor.

" More natural splits
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.

" live substitution
:set inccommand=nosplit
" Rebinds
nmap <C-n> :NERDTreeToggle<CR>

autocmd FileType json syntax match Comment +\/\/.\+$+

let g:python2_host_prog = '/usr/bin/python'

if has('macunix')
  source ~/.vim/macos.vim
  let g:python3_host_prog = '/usr/local/bin/python3'
endif

if has('unix')
  let g:python3_host_prog = '/usr/bin/python3'
endif
