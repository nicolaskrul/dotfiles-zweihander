#!/bin/sh


# send all steam games to certain monitor
steamlib=/games/SteamLibrary    # path to your steamlibrary
mygames=$(ls $steamlib/steamapps/appmanifest_*.acf | sed 's/[^0-9]*//g')    # this sed command removes everything but the digits

for game in $mygames; do
    bspc rule -a steam_app_$game desktop=^10 state=fullscreen
done
