package util

import (
	"bytes"
	"io"
	"log"
	"os"
	"testing"
)

func TestLogErr(t *testing.T) {
	//force an error
	testErr := io.EOF

	// create a custom buffer
	var buf bytes.Buffer
	// set log output to buffer
	log.SetOutput(&buf)

	// reset log output to os.Stderr when testLogErr completes
	defer func() {
		log.SetOutput(os.Stderr)
	}()

	// execute LogErr
	LogErr(testErr)

	// print buf Buffer to test logger to see contents of log
	t.Log(buf.String())
}
