#!/bin/bash

distroname=""
distro=""
packagemanager=""
installcommand=""


install_go_ubuntu() {
	sudo add-apt-repository ppa:longsleep/golang-backports -y
	sudo apt-get update -y
	sudo apt-get install golang-go -y
}

install_go_fedora() {
	sudo dnf install golang
}


check_distro() {
	arch=$(uname -m)
	kernel=$(uname -r)
	if [ -n "$(command -v lsb_release)" ]; then
		distroname=$(lsb_release -s -d)
	elif [ -f "/etc/os-release" ]; then
		distroname=$(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME=//g' | tr -d '="')
	elif [ -f "/etc/debian_version" ]; then
		distroname="Debian $(cat /etc/debian_version)"
	elif [ -f "/etc/redhat-release" ]; then
		distroname=$(cat /etc/redhat-release)
	else
		distroname="$(uname -s) $(uname -r)"
	fi

	IFS=' ' read -r -a array <<< "$distroname"
	distro="${array[0]}"
	echo "Distro detected: $distroname"
}

install_go() {
	if [ "$distro" == "Fedora" ]; then
		install_go_fedora
	elif [ "$distro" == "Ubuntu" ]; then
		install_go_ubuntu
	else
		echo "No installation performed"
	fi

}

config_go() {
	mkdir -p $HOME/go
	echo 'export GOPATH=$HOME/go' >> $HOME/.bashrc
	source $HOME/.bashrc
	echo "Go configured for ${USER}"
}

check_distro
install_go
config_go

cp -r 
go run main.go
