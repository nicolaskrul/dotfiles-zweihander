package main

import (
	"bufio"
	"fmt"
	"github.com/manifoldco/promptui"
	"io/ioutil"
	zhUtil "nicolaskrul.nl/dotfiles/zweihander/util"
	"os"
	"os/exec"
	"os/user"
)

var packageMan string = "apt"
var distro string = "Ubuntu"

func main() {
	clearScreen()
	// getDotFiles()
	displayLogo()
	distro = mainMenu()
	installPackages()
}

func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	zhUtil.LogErr(err)
}

func getDotFiles() {

	// if not empty, git pull
	usr, er := user.Current()
	zhUtil.LogErr(er)
	var repo = "https://gitlab.com/nicolaskrul/dotfiles.git"

	cmd := exec.Command("git", "clone", repo)
	cmd.Dir = "" + usr.HomeDir + "/Development"
	err := cmd.Run()

	zhUtil.LogErr(err)
}

func mainMenu() string {
	prompt := promptui.Select{
		Label: "Select Distribution",
		Items: []string{"Ubuntu", "Fedora"},
	}

	_, result, err := prompt.Run()
	zhUtil.LogErr(err)
	fmt.Printf("%q selected \n", result)
	return result
}

func displayLogo() {
	file, err := ioutil.ReadFile("logo.txt")
	zhUtil.LogErr(err)
	text := string(file)
	fmt.Print(text)
}

func installPackages() {
	if distro == "Fedora" {
		packageMan = "dnf"
	}

	cmdName := "sudo"
	cmdArgs := []string{packageMan, "update"}
	fmt.Println("Updating package list")
	runCommandWithOutput(cmdName, cmdArgs)

	packageList, err := readLines("packages.txt")
	zhUtil.LogErr(err)

	cmdName = "sudo"
	cmdArgs = append([]string{packageMan}, "install")
	cmdArgs = append(cmdArgs, packageList...)
	fmt.Println("Installing packages")
	runCommandWithOutput(cmdName, cmdArgs)
}

func runCommandWithOutput(cmdName string, cmdArgs []string) {
	cmd := exec.Command(cmdName, cmdArgs...)

	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("%s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		os.Exit(1)
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		os.Exit(1)
	}
}

func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// vim			-> ~/.vim
//					-> ~/.config/nvim/
// zhsrc		-> ~/.zshrc
// bspwm    -> ~/.config/bspwm
//					-> bspwm.desktop: usr/share/xsessions/bspwm.desktop
// .xprofile -> ~/.xprofile
// xresources -> ~/.Xresources ??

// func createSymlink() {
// cmdName := "ln"
// cmdPath := "~/.config"
// cmdArgs := []string{"-s", cmdPath}

// runCommandWithOutput(cmdName, cmdArgs)
// }
